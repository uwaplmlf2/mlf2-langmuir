/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* PUGET SOUND BALLASTING MISSION  with EOS Mission at the end */
/* NEED TO SET PARAMETERS SET IN InitializePUGETSOUNDBALLASTEOS */
/* May 20, 2009  */

int next_mode(int nmode, double day_time)
{
    static int icall=0;
    int oldmode,i;
    double x;
    oldmode=nmode;
    if (nmode==MODE_START){
	log_event("next_mode:PUGETSOUNDBALLASTEOS Mission Start\n");
	nmode=MODE_PROFILE_DOWN;
	Steps.cycle=0;
	return(nmode);
    }

    ++icall;
    switch(icall){
        case 1: nmode=MODE_PROFILE_DOWN; 
	    save1=Settle.timeout;/* captive dive  - short */
	    Settle.timeout=Steps.time0;
	    break;
        case 2: nmode=MODE_SETTLE;break;
        case 3: nmode=MODE_DRIFT_SEEK;
		Drift.timeout_sec=Drift.time2;   /* very short test drift mode */
		Drift.closed_time=Drift.timeout_sec+100.; /* don't open drogue in captive dive */
		break;
	case 4: nmode=MODE_PROFILE_UP;break;
	case 5: nmode=MODE_COMM;break;      /* Option to terminate here or remove linefloat */
	case 6: nmode=MODE_PROFILE_DOWN; 
	    Settle.timeout=save1; /* restore long settle */
	    break;
        case 7: nmode=MODE_SETTLE;break;
        case 8: nmode=MODE_DRIFT_SEEK;
		Drift.timeout_sec=1000.;  /* longer test drift mode */
		Drift.closed_time=200.;           /* open part way through */
		break;
	case 9: nmode=MODE_PROFILE_UP;break;
	case 10: nmode=MODE_COMM;break;   /* Pick up float here */
	case 11: if (stage!=2){           /* must set stage==2 to continue */
		nmode=MODE_DONE;
		}
		
/********** if stage is set to 2 at COMM, then mission continues as EOS, else finishes  ***********/
		else{
			log_event("next_mode: EOS MISSION START (after Ballast)\n");
			nmode=MODE_PROFILE_DOWN;   /*initial down/up */
			Down.Pmax=Steps.z4;   /* Ready for full depth UP profile */
			Steps.cycle=Steps.cycle+1;  /* reduce sampling and thus power */
		}
		break;
	case 12: nmode=MODE_PROFILE_UP;
		Mlb.point=0;  /* initialize recorder */
		Mlb.record=1;  /* Start recording */
		log_event("Start Recording upcast\n");
		Up.surfacetime=100.;  /* add a few surface points*/
		break;
	case 13: nmode=MODE_COMM;
		Mlb.record=0;  /* stop recording */
		/*  Testing  
		for (i=1;i<Mlb.point-1;++i){
			printf("        %4.1f   %6.3f\n",Mlb.Psave[i],Mlb.Sigsave[i]);
		}*/
		break; 
	case 14: nmode=MODE_SETTLE;   /* Settle #1 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z1);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#1: %6.3f db %6.3f sigma\n",Steps.z1,x);
		}
		else {    /* if z2sigma() fails use Ballast.Target from initial ballasting */
			log_event("ERROR: next_mode No new target#1, use %6.3g\n",Ballast.Target);
			}
		Settle.SetTarget=2;  /* set target to Ballast.Target */
		Settle.timeout=Steps.time1; 
		Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
		Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
		break;
	case 15: nmode=MODE_SETTLE;   /* Settle #2 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z2);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#2: %6.3f db %6.3f sigma\n",Steps.z2,x);
		}
		else {
			log_event("ERROR: next_mode No new target#2, use %6.3g\n",Ballast.Target);
			}
		Settle.SetTarget=2;
		Settle.timeout=Steps.time2; 
		Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
		Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
		break;
	case 16: nmode=MODE_SETTLE;   /* Settle #3 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z3);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#3: %6.3f db %6.3f sigma\n",Steps.z2,x);
		}
		else {
			log_event("ERROR: next_mode No new target#3, use %6.3g\n",Ballast.Target);
			}
		Settle.SetTarget=2;
		Settle.timeout=Steps.time3; 
		Settle.seek_time=Settle.timeout/2.;  /* half of seeking at full power */
		Settle.decay_time=Settle.seek_time*0.6;  /* decay seeking to 20% by end */
		break;
	case 17: nmode=MODE_SETTLE;   /* Settle #4 */
		pMlb=&Mlb;
		x=z2sigma(pMlb,Steps.z4);  /* Get new target from saved profile */
		if (x>0){
			Ballast.Target=x;
			log_event("Settle Target#4: %6.3f db %6.3f sigma\n",Steps.z4,x);
		}
		else {
			log_event("ERROR: next_mode No new target#4, use %6.3f\n",Ballast.Target);
			}
		Settle.SetTarget=2;
		Settle.timeout=Steps.time4; 
		Settle.seek_time=Settle.timeout;  /* Deep, low N, full power seek */
		Settle.decay_time=200;  
		break;
	case 18: nmode=MODE_PROFILE_DOWN;   /*Ready for next profile */
		Down.Pmax=Steps.z4+5;  /* Dip below bottom point */
		icall=11;      /* Repeat EOS cycle  on next cycle*/
		break;
        default: nmode=MODE_ERROR;
		log_event("ERROR: next_mode Cant get here %d\n",icall);
		break;  /* can't get here */
    }
    log_event("next_mode: %d ->%d\n",oldmode,nmode);
    return(nmode);
}

#include "SampleHURR09.c"
