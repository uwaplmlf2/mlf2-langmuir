cmake_minimum_required(VERSION 3.13)

set(SRC_FILES ballast.c
  sw.c
  labsimtest.c
  errors.c)

find_package(libtt8 REQUIRED)
find_package(libmlf2 REQUIRED)

add_library(ballast STATIC ${SRC_FILES})
target_sources(ballast INTERFACE ballast.h ballastvers.h)
target_include_directories(ballast PUBLIC
  ${CMAKE_BINARY_DIR}/generated
  ${LIBTT8_INCLUDE_DIR}
  ${LIBMLF2_INCLUDE_DIR})
